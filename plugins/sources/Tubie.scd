// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin
*/


~name = \Tubie;
~description = "Currugated tube... kinda";
~channelLayout = \mono;

~function = {|freq = 200, freq_sc = 1, amp = 0.7, rq = 0.1, gain = 2.4|
	var snd;
	var saw1, pulse, reso, verb, out;
	pulse = Clipper8.ar(SinOsc.ar(SinOsc.ar(freq), mul: LFNoise1.kr(1/freq).range(15,25)), -0.5, 0.5);
	saw1 = Saw.ar(LFNoise1.ar(1/freq).range(freq-21, freq+17), mul: 0.9) * pulse;
	reso = RHPF.ar(saw1, freq*12, rq);
	reso = MoogFF.ar(reso, SinOsc.kr(0.1).range(40, 12000), gain, mul: amp);
	reso = DynKlank.ar(`[
		[
			LFNoise1.kr(freq*0.01).range(freq*3-10, freq*3+10),
			LFNoise1.kr(freq*0.01).range(freq*5-10, freq*5+10),
			LFNoise1.kr(freq*0.01).range(freq*11-10, freq*11+10),
			LFNoise1.kr(freq*0.01).range(freq*7-10, freq*7+10)
		],[0.02, 0.04, 0.03, 0.02],[0.5, 0.6, 0.6, 0.73]], saw1, freqscale: freq_sc);
	verb = FreeVerb.ar(reso, 0.8, 0.7, 0.9, Line.kr(0, 0.6, 0.5, mul: 0.7));
	out = BLowPass.ar(verb, SinOsc.kr(0.2).range(100, 9000), rq) * amp;
	LeakDC.ar(out);
};
