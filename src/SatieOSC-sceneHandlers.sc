+ SatieOSC {
	postProcessorSetSimpleHandler {
		^{ |args|
			var key = args[1];
			var val = args[2];
			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			switch (key)
			{\outputIndex} {satie.postProcStruct.put(\outputIndex, val)}
			{\spatializerNumber} {satie.postProcStruct.put(\spatializerNumber, val)};
		}
	}

	postProcessorSetArrayHandler {
		^{ |args|
			var key = args[1];
			var vals = args.copyRange(2, args.size - 1);
			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			switch (key)
			{\pipeline} {satie.postProcStruct.put(\pipeline, vals)}
			{\synthArgs}{satie.postProcStruct.put(\synthArgs, vals)};
		}
	}

	postProcessorApplyHandler {
		^{ |args|
			var key = args[1];
			var val = args[2];
			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			satie.replacePostProcessor(
				satie.postProcStruct[\pipeline],
				outputIndex: satie.postProcStruct[\outputIndex],
				spatializerNumber: satie.postProcStruct[\spatializerNumber],
				synthArgs: satie.postProcStruct[\synthArgs]
			);
		}
	}
	ambiPostProcessorSetSimpleHandler {
		^{ |args|
			var key = args[1];
			var val = args[2];
			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			switch (key)
			{\outputIndex} {satie.ambiPostProcStruct.put(\outputIndex, val)}
			{\spatializerNumber} {satie.ambiPostProcStruct.put(\spatializerNumber, val)}
			{\order} {satie.ambiPostProcStruct.put(\order, val)};
		}
	}

	ambiPostProcessorSetArrayHandler {
		^{ |args|
			var key = args[1];
			var vals = args.copyRange(2, args.size - 1);
			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			switch (key)
			{\pipeline} {satie.ambiPostProcStruct.put(\pipeline, vals)}
			{\synthArgs}{satie.ambiPostProcStruct.put(\synthArgs, vals)};
		}
	}

	ambiPostProcessorApplyHandler {
		^{ |args|
			var key = args[1];
			var val = args[2];
			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			satie.replaceAmbiPostProcessor(
				satie.ambiPostProcStruct[\pipeline],
				order: satie.ambiPostProcStruct[\order],
				outputIndex: satie.ambiPostProcStruct[\outputIndex],
				spatializerNumber: satie.ambiPostProcStruct[\spatializerNumber],
				synthArgs: satie.ambiPostProcStruct[\synthArgs]
			);
		}
	}

	postProcessorPipelineSetProperties {
		^{ |args|
			var targetName, props, target;
			targetName = args[1];
			props = args.copyRange(2, args.size - 1);
			if (satie.debug, {"→ %: message: %".format(this.class.getBackTrace, args).postln});
			target = this.getPostProcessorInstance(targetName.asSymbol);
			if (target.isNil,
				{
					"%: PostProcessor % does not exist".format(this.class.getBackTrace, targetName).error
				},
				{
					props.pairsDo({ |prop, val|
						target.set(prop, val);
					})
				})
		}
	}
}
