TITLE:: SatieGUI
summary:: Basic SATIE viewer for speaker location
categories:: Libraries
related:: Overview/SATIE-Overview, Classes/Satie

DESCRIPTION::
This class is used to create 2 windows (top and front views) that aid users to visualize the speaker's positions on SATIE's spatializers. This class currently works only with VBAP-based spatializers.

Users can click on the speakers to make them invisible and check if there are other speakers hidden "below" it.

CLASSMETHODS::

method:: new
Create a SatieGUI object, allowing requesting SATIE to create and draw the views.

argument:: satieContext
A SATIE renderer. The created SatieGUI object will only be able to request information from that specific renderer.

INSTANCEMETHODS::

method:: drawViews
Create 2 windows and populate with the speaker's locations.

EXAMPLES::

code::

(
s = Server.supernova.local;

~satieConfiguration = SatieConfiguration.new(s, [\stereoListener, \dodecahedronVBAP], outBusIndex: [0, 2]);

/* Alternativelly, the above line can be commented out 
 * and replaced by the following one set SATIE to 
 * initialize a single spatializer
 */

// ~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);


~satieConfiguration.spatializers.keys;
~satie = Satie.new(~satieConfiguration);
~satie.waitForBoot({
    ~satie.makeSourceInstance(\testSynth, \dustyRez, \default, synthArgs: [\density, 15, \attack, 25]);
});
)

// Instantiate the GUI class
~visualizer = SatieGUI.new(~satie);

// request the current spatializer's drawings
~visualizer.drawViews;

::