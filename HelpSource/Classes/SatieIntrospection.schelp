TITLE:: SatieIntrospection
summary:: Basic SATIE Introspection
categories:: Libraries
related:: Overview/SATIE-Overview, Classes/Satie

DESCRIPTION::
This class is used to inspect much of the internal structure of SATIE, but also to get information on running synths, including current argument values. It is also possible to get basic info on SATIE's spatializers.

SATIE's introspection does not implement a method to list running synths, but relies on the user to keep track of the instantiated synths.
Users can always query if one of the instatiated synths is playing or running by using link::Overviews/Methods#isRunning#isRunning:: or link::Overviews/Methods#isPlaying#isPlaying:: methods. Also, check link::Classes/NodeWatcher:: for more information.

CLASSMETHODS::

method:: new
Create a SatieIntrospection object, allowing further introspection requests to the correspondent SATIE renderer specified as argument. It is being used by link::Classes/Satie:: and is user-accessible via link::Classes/Satie#*inspector::.

argument:: satieContext
A SATIE renderer. The created SatieIntrospection object will only be able to request information from that specific renderer.

INSTANCEMETHODS::

method:: getCurrentConfig
Get SATIE's current configuration.

method:: getValue
Get a single argument value for a running SATIE Synth. Similar to the Synth get method, if an inexistent control value is provided, the method will return 0.0 as the answer. Invalid nodemane values will return empty dictionaries. This method should not be used to mapping control parameters in real time as, similar to the link::Overviews/Methods#get#get method::, it is based on OSCFunc and works assincronously.

argument:: instanceName
Source instance name used when the synth was created using the link::Classes/Satie#-makeSourceInstance:: method.

argument:: control
The Synth Control (parameter) to be requested (expect a symbol).

argument:: group
Group name used when the synth was created using the link::Classes/Satie#-makeSourceInstance:: method.

method:: getDefValue
Get a single argument value for a running SATIE Synth. Similar to the Synth get method, if an inexistent control value is provided, the method will return 0.0 as the answer. Invalid nodemane values will return empty dictionaries. This method should not be used to mapping control parameters in real time as, similar to the link::Overviews/Methods#get#get method::, it is based on OSCFunc and works assincronously.

argument:: nodename
Synth reference/pointer. Accepts SATIE's sound instance references (Check examples on how to reference SATIE Synths).

argument:: control
The Synth Control (parameter) to be requested (expect a symbol).

method:: getAllValues
Get a list of arguments and values for a running SATIE Synth. This method returns only the control parameters atributed to that synth and does not include spatialization parameters. Similar to getValue, invalid nodemane values will return an error message. This method should not be used to mapping control parameters in real time as, similar to the link::Overviews/Methods#get#get method::, it is based on OSCFunc and works assincronously.

argument:: instanceName
Source instance name used when the synth was created using the link::Classes/Satie#-makeSourceInstance:: method.

argument:: group
Group name used when the synth was created using the link::Classes/Satie#-makeSourceInstance:: method.

method:: getSynthValues
Get all argument values for a running SATIE Synth. Similar to getValue, invalid nodemane values will return an error message. This method should not be used to mapping control parameters in real time as, similar to the link::Overviews/Methods#get#get method::, it is based on OSCFunc and works assincronously.

argument:: instanceName
Source instance name used when the synth was created using the link::Classes/Satie#-makeSourceInstance:: method.

argument:: returnFunction
Optional: Function to be executed when the server answers the request. The dictionary containing all running synths and control parameters is passed as an argument to the evaluated function.

method:: getAllSynths
Get all argument values for all running SATIE Synths. This method should not be used to mapping control parameters in real time as, similar to the link::Overviews/Methods#get#get method::, it is based on OSCFunc and works assincronously.

argument:: returnFunction
Optional: Function to be executed when the server answers the request. The dictionary containing all running synths and control parameters is passed as an argument to the evaluated function.

method:: getSynthParameters
Get a list with all arguments for a running SATIE Synth. Different from getValue, invalid teletype::instanceName:: values will return an error message. This method relies on SATIE's SynthDef info and does not request information directly from the running Synth.

argument:: instanceName
Source instance name used when the synth was created using the link::Classes/Satie#-makeSourceInstance:: method.

method:: getDefaultSynthParameters
Get all arguments and default values for a running SATIE Synth. Different from getValue, invalid nodemane values will return an error message. This method relies on SATIE's SynthDef info and does not request information directly from the running Synth.

argument:: instanceName
Source instance name used when the synth was created using the link::Classes/Satie#-makeSourceInstance:: method.

method:: getSpeakerAngles
Get current spatializer's speaker angles. Returns a dictionary with the spatializer's name and speaker array.

method:: getSpatializerSpeakerAngles
Get all available spatializer's speaker angles. Returns a dictionary with the spatializer's name and speaker array.

argument:: spatializerName
Spatializer's name. Accepts Symbols or Strings.

method:: getPluginList
Returns a dictionary audio plugins. Key is the type of plugin, value a Set of names.

method:: getPluginArguments
Returns an Array with argument and value pairs.

argument:: plugin
Name of the plugin. Accepts Symbols or Strings.

method:: getPluginDescription
Returns the plugin description.

argument:: plugin
Name of the plugin. Accepts Symbols or Strings.

method:: getPluginInfo
Returns a Dictionary with all plugin information.

argument:: plugin
Name of the plugin. Accepts Symbols or Strings.

method:: getPluginInfoJSON
Returns all plugin information in JSON format.

argument:: plugin
Name of the plugin. Accepts Symbols or Strings.

EXAMPLES::

code::

(
// define a server, SATIE needs it
s = Server.supernova.local;
// instantiate a SatieConfiguration. Here we will use a stereo spatializer
~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
// list possibleL listeners:
~satieConfiguration.spatializers.keys;
// instantiate SATIE renderer and pass it the configuration
~satie = Satie.new(~satieConfiguration);
~satie.waitForBoot({
    // Instantiate Satie Introspection
    ~introspection = SatieIntrospection(~satie);

	// create a single synth in the default group
	~satie.makeSourceInstance(\testSynth, \dustyRez, \default, synthArgs: [\density, 15, \attack, 25]);

	// set a random frequency
	~satie.groupInstances[\default][\testSynth].set(\freq, rrand(200, 2000));
})
)

// Query if specific synth is running using NodeWatcher
("testSynth running? " + ~satie.groupInstances[\default][\testSynth].isRunning).postln;

// Get freq values from Synth using the default get method
~satie.groupInstances[\default][\testSynth].get(\freq, { arg value; ("freq is now:" + value + "Hz").postln; });

// Get freq values from Synth using the Satie's introspection
//     Post values and return a dictionary that will be populated
//     After the server returns the current argument value
a = ~introspection.getValue(\testSynth, \freq);

// The stored dictionary can be accessed after the value is returned
a.postln;

// The list (dictionary) of the default values for a given running 
// synth can be requested using getDefaultSynthParameters
~introspection.getDefaultSynthParameters(\testSynth);

// It's also possible to set the default frequency value for a synth
~introspection.getDefValue(\freq, \testSynth);

// Get current spatializer speaker angles
~introspection.getSpeakerAngles;

// Also get any available spatializer speaker angles
~introspection.getSpatializerSpeakerAngles(\domeVBAP);


::