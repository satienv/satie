TestSatieSynthDesc_Server : SatieUnitTest {

	var satie, config, server;

	setUp {
		server = Server(this.class.name);
		config = SatieConfiguration(
			server: server,
			listeningFormat: [\stereoListener],
			outBusIndex: [0],
			ambiOrders: [3],
		);
		satie = Satie(config);

		this.boot(satie);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_synthDescLib_notEmpty {
		this.assert(
			satie.synthDescLib.synthDescs.notEmpty,
			"After boot, Satie's SynthDescs should no longer be empty"
		);
	}

	test_plugin_synthDefs {
		var defs;
		var lib = satie.synthDescLib;

		defs = lib.synthDescs.keys.asArray;
		satie.getAllPlugins[\sources].keysValuesDo { |key, plug|
			var name = plug.name;

			// \mono channelLayout means there will also be a kamikaze version
			if(plug.channelLayout === \mono) {
				[name, (name ++ "_kamikaze").asSymbol].do { |name|
					this.assert(defs.includes(name), "SynthDescLib contains %".format(name));
				};
			};
			// all SynthDefs should exist in an Ambi3 version
			name = (name ++ "Ambi3").asSymbol;
			this.assert(defs.includes(name), "SynthDescLib contains %".format(name));
		}
	}

}

