TestSatieConfiguration : SatieUnitTest {

	var server, config, jsonPath;

	setUp {
		var dataDir = this.class.filenameSymbol.asString.dirname +/+ "data";

		jsonPath = (
			valid: dataDir +/+ "valid.json",
			invalid: dataDir +/+ "invalid.json",
		);
		server = Server(this.class.name);
		config = SatieConfiguration(server);
	}

	tearDown {
		server.remove;
	}

	test_init {
		var config = SatieConfiguration.init;
		this.assertEquals(config.class, SatieConfiguration, "*init should create a basic instance of SatieConfiguration");
		this.assertEquals(config.server, Server.default, "*init should set the server instance variable to Server.default when none provided");
	}

	test_new {
		var arguments = [
			server,
			[\stereoListener, \domeVBAP], // listeningFormat
			8, // numAudioAux
			[0, 12], // outBusIndex
			[1, 3], // ambiOrders
			2, // minOutputBusChannels
		];
		var config = SatieConfiguration.new(*arguments);
		var values = [
			config.server,
			config.listeningFormat,
			config.numAudioAux,
			config.outBusIndex,
			config.ambiOrders,
			config.minOutputBusChannels,
		];
		this.assertEquals(values, arguments, ".new() should copy provided argument values to their corresponding instance variables");
	}

	test_init_configure {
		var arguments = [
			[\stereoListener, \domeVBAP], // listeningFormat
			8, // numAudioAux
			[0, 12], // outBusIndex
			[1, 3], // ambiOrders
			2, // minOutputBusChannels
		];
		var config = SatieConfiguration.init(server).configure(*arguments);
		var values = [
			config.listeningFormat,
			config.numAudioAux,
			config.outBusIndex,
			config.ambiOrders,
			config.minOutputBusChannels,
		];
		this.assertEquals(values, arguments, "*init().configure() should copy provided argument values to their corresponding instance variables");
	}

	test_init_server {
		var config = SatieConfiguration.init(server);
		this.assertEquals(config.server, server, "*init(server) should set the server instance variable");
	}

	test_default_arg_values {
		var defaults = (listeningFormat: [\stereoListener], numAudioAux: 0, outBusIndex: [0], ambiOrders: [], minOutputBusChannels: 2);
		var initConfig = SatieConfiguration.init().configure();
		var newConfig = SatieConfiguration.new();
		defaults.keysValuesDo { |key, value|
			this.assertEquals(initConfig.perform(key), value, "*init().configure() default value for % should be correct".format(key));
			this.assertEquals(newConfig.perform(key), value, "*new() default value for % should be correct".format(key));
		};
	}

	test_ambiOrders_exceptions {
		var tests = [
			[],
			[0, 3],
		];
		tests.do { |i|
			this.assertNoException(
				{ SatieConfiguration(server, ambiOrders: [0, 3]) },
				"No Exception should be thrown when providing an invalid ambisonic order"
			)
		};

		tests = [
			[0, 3, 5],
			[0, 3, 2.0],
		];
		tests.do { |i|
			this.assertException(
				{ SatieConfiguration(server, ambiOrders: i) },
				Error,
				"Exception should be thrown when providing an invalid ambisonic order"
			)
		}
	}

	test_server_nil {
		var config = SatieConfiguration(server: nil);
		this.assertEquals(config.server, Server.default, "Leaving the server argument Nil should choose Server.default")
	}

	test_satieRoot {
		var rootPath = Satie.class.filenameSymbol.asString.dirname.dirname;
		this.assertEquals(config.satieRoot, rootPath, "Satie root path is correctly set");
	}

	test_userSupportDir {
		var userPath = Platform.userHomeDir +/+ ".local/share/satie";
		this.assertEquals(config.satieUserSupportDir, userPath, "User support path is correctly set");
	}

	test_hoaEncoderType {
		this.assertEquals(config.hoaEncoderType, \wave, "Default hoaEncoderType is \\wave");
		config.hoaEncoderType = \harmonic;
		this.assertEquals(config.hoaEncoderType, \harmonic, "hoaEncoderType was correctly set to \\harmonic");
	}

	test_hoaEncoderType_Error {
		var error = { config.hoaEncoderType = \foo };
		this.assertException(error, Error, "Setting hoaEncoderType to invalid value throws an error");
	}

	test_initDicts {
		var dicts = [
			\sources,
			\effects,
			\processes,
			\spatializers,
			\mappers,
			\postprocessors,
			\hoa,
			\monitoring,
		];
		dicts.do { |key|
			this.assertEquals(
				config.slotAt(key).class.asSymbol,
				\SatiePlugins,
				"Plugin dictionary % instantiated".format(key)
			)
		}
	}

	test_loadPlugins {
		var plugin_dirs = [
			\sources,
			\effects,
			\processes,
			\spatializers,
			\mappers,
			\postprocessors,
			\hoa,
			\monitoring,
		];
		plugin_dirs.do { |directory|
			var path = PathName(config.satieRoot +/+ "plugins" +/+ directory);
			path.filesDo { |file|
				var folder = file.folderName.asSymbol;
				var filename = file.fileNameWithoutExtension.asSymbol;
				this.assertEquals(
					config.slotAt(folder).at(filename).notNil,
					true,
					"Plugin %/% loaded successfully".format(folder, filename))
			}

		}
	}

	test_fromJsonFile {
		var jsonConfig;

		this.assertNoException(
			{ jsonConfig = SatieConfiguration.fromJsonFile(jsonPath.valid); },
			"SatieConfiguration.fromJsonFile should not throw an Error when JSON is valid"
		);

		this.assertEquals(
			jsonConfig.server,
			Server.default,
			"Server should be the default when JSON server.name was nil"
		);

		this.assertEquals(
			Server.program.contains("supernova"),
			false,
			"JSON server.supernova should set whether supernova is used"
		);

		this.assertEquals(
			jsonConfig.listeningFormat,
			[\stereoListener, \domeVBAP],
			"JSON listeningFormat should set the configuration's listeningFormat"
		);

		this.assertEquals(
			jsonConfig.outBusIndex,
			[0, 2],
			"JSON outBusIndex should set the configuration's outBusIndex"
		);

		this.assertEquals(
			jsonConfig.ambiOrders,
			[1, 3],
			"JSON ambiOrders should set the configuration's ambiOrders"
		);

		this.assertEquals(
			jsonConfig.numAudioAux,
			2,
			"JSON numAudioAux should set the configuration's numAudioAux"
		);

		this.assertEquals(
			jsonConfig.minOutputBusChannels,
			2,
			"JSON minOutputBusChannels should set the configuration's minOutputBusChannels"
		);
	}

	test_fromJsonFile_notFound {
		this.assertException(
			{ SatieConfiguration.fromJsonFile("/file/does/not/exist.json"); },
			Error,
			"SatieConfiguration.fromJsonFile should throw an Error when file is not found"
		);
	}

	test_fromJson_string_arg {

		this.assertException(
			{ SatieConfiguration.fromJsonFile('a Symbol') },
			Error,
			"SatieConfiguration.fromJsonFile should throw an error when not passed a String"
		);

		this.assertException(
			{ SatieConfiguration.fromJsonString('a Symbol') },
			Error,
			"SatieConfiguration.fromJsonString should throw an error when not passed a String"
		);
	}

	test_initJson_server_nil {
		// fake a JSON config using an Event
		var json = (
			server: nil,
			listeningFormat: [\stereoListener],
			outBusIndex: [0],
			numAudioAux: 0,
			ambiOrders: [1],
			minOutputBusChannels: 2,
			userPluginsDir: nil
		);
		var jsonConfig = SatieConfiguration.initJson(json);

		this.assertEquals(
			jsonConfig.server,
			Server.default,
			"A nil server value should revert to using the default server"
		);
	}

}
