//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

~name = \MonoIn;
~description = "A direct input";
~channelLayout = \mono;

~function = {
	|gate = 0, bus = 0, attack = 0.02, susL = 0.9, release = 0.1, curve = -4|
	var env = Env.asr(attack, susL, release, curve);
	SoundIn.ar(bus, EnvGen.kr(env, gate));
}