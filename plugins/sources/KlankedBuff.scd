// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin
*/


~name = \KlankedBuff;
~description = "looped playback through a bank of klank";
~channelLayout = \mono;

~function =  { | bufnum = 0, startPos = 0, rate = 1, tone=1, loop = 1, trimDB=0, freq = 200, rq = 0.1, attack = 2, dec = 0.3, sus = 0.5, rel = 6, amp = 1, gate = 0, win = \sin, revMix = 0.5, revRoom = 1, revDamp = 0.5|

	var env, envGen, resonance, verb;
	var lpassFq, sig;

	tone = abs (tone);
	tone = tone.clip(0,1);

	env = Env.adsr(attack, dec, sus, rel);
	envGen = EnvGen.kr(env, gate, doneAction: Done.freeSelf);

	lpassFq = 100 + (tone**(0.7) * 21900) ;
	sig = trimDB.dbamp * PlayBuf.ar(1, bufnum, rate: BufRateScale.kr(bufnum) * rate, startPos: startPos.linlin(0, 1, 0, BufFrames.kr(bufnum)), loop: loop);
	sig = BLowPass.ar(sig, Lag.kr(lpassFq, 0.02));
	sig = BPF.ar(sig, freq, rq);


	resonance = DynKlank.ar(`[
		[
			LFNoise1.kr(freq*0.01).range(117, 121),
			LFNoise1.kr(freq*0.01).range(327, 332),
			LFNoise1.kr(freq*0.01).range(529, 534),
			LFNoise1.kr(freq*0.01).range(745, 750),
			LFNoise1.kr(freq*0.01).range(927, 932),
			LFNoise1.kr(freq*0.01).range(1168, 1179),
			LFNoise1.kr(freq*0.01).range(1135, 1140),
			LFNoise1.kr(freq*0.01).range(1390, 1405),
			LFNoise1.kr(freq*0.01).range(1567, 1573),
		],
		[
			LFNoise1.kr(1).range(-30, -20).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-30, -25).dbamp,
			-20.dbamp;
			-10.dbamp;
			-25.dbamp;
		],// amps
		[
			0.8,
			0.8,
			0.4,
			0.3,
			0.4,
			0.4,
			0.8,
			0.8,
			0.5
		]   // ring times
	],
	sig);
	verb = FreeVerb.ar(sig, revMix, revRoom, revDamp, Line.kr(0, 0.6, 0.5, mul: 0.7));
	verb = LeakDC.ar(verb);
	verb = verb * envGen * amp;
	Limiter.ar(verb, 0.7);
};

~setup = {}
