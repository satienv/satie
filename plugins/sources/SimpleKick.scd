// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin
*/

// SynthDef by Victor Comby


~name = \SimpleKick;
~description = "A Kick generated from a fast pitch bended SinOsc, and a bit of white noise";
~channelLayout = \mono;

~function =  {
	var kicksig, kickenv, transient, transienv, kickfinal, kickfreq;
	kickfreq = XLine.ar(500, 50, 0.08);
	kickenv = EnvGen.ar(Env.adsr(0.01, 0.2, 0, 0, 1, -4), doneAction:0);
	transienv = EnvGen.ar(Env.adsr(0.01, 0.05, 0, 0, 1, -4), doneAction:0);
	kicksig = SinOsc.ar(kickfreq)*kickenv;
	transient = WhiteNoise.ar(0.5)*transienv;
	transient = BBandPass.ar(transient, 1000, 0.5);
	transient = LPF.ar(transient, 400);
	kickfinal = kicksig + transient
};


~setup = {}
