// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

~name = \sine;
~description = "A simple sine wave with low pass filter roll-off";
~channelLayout = \mono;

~function = {
	|
	frequency = 200,
	attack = 0.1,
	decay = 0.1,
	sustain = 1.0,
	release = 1,
	amplitude = 1.0,
	cutoff_freq = 100.0,
	gate = 1
	|
	var snd, env, envGen;
	snd = LPF.ar(FSinOsc.ar(frequency), cutoff_freq);
	env = Env.adsr(attack, decay, sustain, release);
	envGen = EnvGen.kr(env, gate, doneAction: Done.freeSelf);
	snd * envGen * amplitude
};
