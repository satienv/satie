TestIntrospection : SatieUnitTest {

    var server, satie, introspection, source;

	setUp {
		server = Server(this.class.name);
		satie = Satie(SatieConfiguration(server));
        introspection = SatieIntrospection(satie);
		this.boot(satie);
        source = satie.makeSourceInstance(\testSynth, \MonoIn, \default);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

    test_getCurrentConfig {
        var expected_answer = Dictionary.newFrom([\listeningFormat,[\stereoListener],\outBusIndex,[0],\numAudioAux,0,\server,\NULL,\ambiOrders,[ ],\minOutputBusChannels,2]);
        var answer = introspection.getCurrentConfig;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getCurrentConfig: returns a dictionary with the current config."
        );
	}

    test_getValue {
        var expected_answer = Dictionary.newFrom([\gainDB, -99.0]);
        var answer = introspection.getValue(\testSynth,\gainDB,\default);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getValue: returns a single argument value for a running SATIE Synth."
        );
	}

    test_getDefValue {
        var expected_answer = Dictionary.newFrom([\curve, -4.0]);
        var answer = introspection.getDefValue(\curve,\testSynth);
        1.wait;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getDefValue: returns a single argument value for a running SATIE Synth."
        );
	}

    test_getAllValues {
        var expected_answer = Dictionary.newFrom([\bus,0.0,\susL,0.89999997615814,\release,0.10000000149012,\gate,0.0,\curve,-4.0,\attack,25.0]);
        var answer = introspection.getAllValues(\testSynth);
        1.wait;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getAllValues: returns all argument values for a running SATIE Synth."
        );
	}

    test_getSynthParameters {
        var expected_answer = Dictionary.newFrom([\testSynth,[\curve,\susL,\gate,\bus,\attack,\release]]);
        var answer = introspection.getSynthParameters(\testSynth);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getSynthParameters: returns a list with all arguments for a running SATIE Synth."
        );
	}

    test_getDefaultSynthParameters {
        var expected_answer = Dictionary.newFrom([\bus,0,\susL,0.9,\release,0.1,\gate,0,\curve,-4,\attack,0.02]);
        var answer = introspection.getDefaultSynthParameters(\testSynth);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getDefaultSynthParameters: returns all arguments and default values for a running SATIE Synth."
        );
	}

    test_getSpeakerAngles {
        var expected_answer = Dictionary.newFrom([\stereoListener,[-90,90]]);
        var answer = introspection.getSpeakerAngles;
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getSpeakerAngles: returns current spatializer's speaker angles."
        );
	}

    test_getSpatializerSpeakerAngles {
        var expected_answer = Dictionary.newFrom([\stereoListener,[-90,90]]);
        var answer = introspection.getSpatializerSpeakerAngles(\stereoListener);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getSpatializerSpeakerAngles: returns all available spatializer's speaker angles."
        );
	}

    test_getPluginList {
        var answer = introspection.getPluginList;
        server.sync;
        this.assert(
            answer[\generators] != nil,
            "SatieIntrospection.getPluginList: returns a dictionary audio plugins."
        );
	}

    test_getPluginArguments {
        var expected_answer = [ \gate, 0, \bus, 0, \attack, 0.02, \susL, 0.9, \release, 0.1, \curve, -4 ];
        var answer = introspection.getPluginArguments(\MonoIn);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getPluginArguments: returns an Array with argument and value pairs."
        );
	}

    test_getPluginDescription {
        var expected_answer = "A direct input";
        var answer = introspection.getPluginDescription(\MonoIn);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getPluginDescription: returns the plugin description."
        );
	}

    test_getPluginInfo {
        var expected_answer = Dictionary.newFrom([\description,"A direct input",\arguments,Dictionary.newFrom([\curve,-4,\susL,0.9,\gate,0,\bus,0,\attack,0.02,\release,0.1])]);
        var answer = introspection.getPluginInfo(\MonoIn);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getPluginInfo: returns a Dictionary with all plugin information."
        );
	}

    test_getPluginInfoJSON {
        var expected_answer = "{ \"MonoIn\": { \"description\": \"A direct input\", \"arguments\": { \"curve\": -4, \"susL\": 0.9, \"gate\": 0, \"bus\": 0, \"attack\": 0.02, \"release\": 0.1 } } }";
        var answer = introspection.getPluginInfoJSON(\MonoIn);
        server.sync;
        this.assertEquals(
            answer,
            expected_answer,
            "SatieIntrospection.getPluginInfoJSON: Returns all plugin information in JSON format."
        );
	}
}

